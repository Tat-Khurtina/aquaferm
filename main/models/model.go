package models

import (
	"time"
)

type Users struct {
	Id         int    `gorm:"primaryKey"`
	Name       string `json:"name"`
	LastName   string `json:"lastName"`
	MiddleName string `json:"middleName"`
	Email      string `json:"email"`
	IdRole     int    `json:"idRole"`
	Password   string `json:"password"`
}

type Roles struct {
	Id   int `gorm:"primaryKey"`
	Role string
}

type Modes struct {
	Id   int `gorm:"primaryKey"`
	Name string
}

type Equipment struct {
	Id        int `gorm:"primaryKey"`
	Name      string
	Included  bool
	IdMode    int
	IdSensors int
}

type Sensors struct {
	Id                    int `gorm:"primaryKey"`
	Name                  string
	IdSystems             int
	Location              string
	MinOperatingCondition float64
	MaxOperatingCondition float64
	MinCriticalCondition  float64
	MaxCriticalCondition  float64
	MinDeath              float64
	MaxDeath              float64
	UnitsOfMeasurement    string
	Frequency             string
	Included              bool
	IdPopulations         int
}

type Systems struct {
	Id          int `gorm:"primaryKey"`
	Name        string
	IdSubtypes  int
	IdRole      int
	IdEquipment int
}

type SubtypesOfSystems struct {
	Id     int `gorm:"primaryKey"`
	Name   string
	IdType int
}

type TypesOfSystems struct {
	Id   int `gorm:"primaryKey"`
	Name string
}

type Indications struct {
	Id        int `gorm:"primaryKey"`
	IdSensors int
	Value     float32
	Datetime  time.Time
}

type Populations struct {
	//Id int `gorm:"uniqueIndex"`
	Id        int `gorm:"primaryKey"`
	Name      string
	StartDate time.Time
	Comment   string
}

type FoodPopulations struct {
	Id           int `gorm:"primaryKey"`
	AmountOfFood string
	Datetime     time.Time
	IdPopulation int
	Food         string
}

type PopulationGrowth struct {
	Id                  int `gorm:"primaryKey"`
	IdPopulation        int
	NumberOfIndividuals string
	MassOfIndividuals   string
	Observation         string
}

type Tasks struct {
	Id          int `gorm:"primaryKey"`
	IdUser      int
	Name        string
	Description string
	StartDate   time.Time
	FinishDate  time.Time
	Status      string
}
