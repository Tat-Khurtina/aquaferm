package config

import (
	"aquaferm/main/models"
	"aquaferm/main/tools"
	"fmt"
	"time"
)

func InitializationSystems() {
	var systems []models.Systems
	systems = append(systems, models.Systems{Name: "B1. Электроснабжение"})
	systems = append(systems, models.Systems{Name: "B2. Теплоснабжение"})
	systems = append(systems, models.Systems{Name: "B7 Прочие системы"})
	systems = append(systems, models.Systems{Name: "B6. Единая среда передачи и хранения данных"})
	systems = append(systems, models.Systems{Name: "B3. Водоснабжение"})
	systems = append(systems, models.Systems{Name: "B4. Водоотведение"})
	systems = append(systems, models.Systems{Name: "B5 Система воздухоснабжения ВК"})
	systems = append(systems, models.Systems{Name: "C1. Система циркуляции водной среды"})
	systems = append(systems, models.Systems{Name: "C2 Система водоочистки"})
	systems = append(systems, models.Systems{Name: "С4 Система обеззараживания"})
	systems = append(systems, models.Systems{Name: "С5 Система кормления"})
	systems = append(systems, models.Systems{Name: "С6 Система аэрации/ оксигенизации"})
	systems = append(systems, models.Systems{Name: "С7 Система озонирования"})
	systems = append(systems, models.Systems{Name: "С8 Система освещения"})
	systems = append(systems, models.Systems{Name: "С9 Система учета аквакультуры"})
	systems = append(systems, models.Systems{Name: "C10 Система контроля зараженности"})
	systems = append(systems, models.Systems{Name: "C11 Система пересадки"})

	Db.Create(&systems)

}

func InitializationSensors() {

	sensors := models.Sensors{
		Id:                    0,
		Name:                  "Температура внутри",
		IdSystems:             7,
		Location:              "",
		MinOperatingCondition: 19,
		MaxOperatingCondition: 23,
		MinCriticalCondition:  14,
		MaxCriticalCondition:  25,
		MinDeath:              5,
		MaxDeath:              30,
		UnitsOfMeasurement:    "Градус Цельсия",
		Frequency:             "",
		Included:              false,
		IdPopulations:         0,
	}
	Db.Create(&sensors)

	sensors = models.Sensors{
		Id:                    0,
		Name:                  "Температура вне",
		IdSystems:             7,
		Location:              "",
		MinOperatingCondition: 10,
		MaxOperatingCondition: 23,
		MinCriticalCondition:  5,
		MaxCriticalCondition:  25,
		MinDeath:              0,
		MaxDeath:              30,
		UnitsOfMeasurement:    "Градус Цельсия",
		Frequency:             "",
		Included:              false,
		IdPopulations:         0,
	}
	Db.Create(&sensors)
	sensors = models.Sensors{
		Id:                    0,
		Name:                  "Влажность",
		IdSystems:             7,
		Location:              "",
		MinOperatingCondition: 40,
		MaxOperatingCondition: 60,
		MinCriticalCondition:  30,
		MaxCriticalCondition:  50,
		MinDeath:              10,
		MaxDeath:              20,
		UnitsOfMeasurement:    "%",
		Frequency:             "",
		Included:              false,
		IdPopulations:         0,
	}
	Db.Create(&sensors)
	sensors = models.Sensors{
		Id:                    0,
		Name:                  "Давление",
		IdSystems:             7,
		Location:              "",
		MinOperatingCondition: 20,
		MaxOperatingCondition: 150,
		MinCriticalCondition:  15,
		MaxCriticalCondition:  155,
		MinDeath:              10,
		MaxDeath:              160,
		UnitsOfMeasurement:    "Па",
		Frequency:             "",
		Included:              false,
		IdPopulations:         0,
	}
	Db.Create(&sensors)

}

func InitializationIndications() {

	for i := 1; i < 20; i++ {
		var value float32
		value = 20.0
		indications := models.Indications{IdSensors: 1, Value: value, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 1, Value: value - value*0.2, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 1, Value: value + value*0.1, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 1, Value: value - value*0.1, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 1, Value: value + value*0.2, Datetime: time.Now()}
		Db.Create(&indications)
	}
	for i := 1; i < 20; i++ {
		var value float32
		value = 15.0
		indications := models.Indications{IdSensors: 2, Value: value, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 2, Value: value - value*0.2, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 2, Value: value + value*0.1, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 2, Value: value - value*0.1, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 2, Value: value + value*0.2, Datetime: time.Now()}
		Db.Create(&indications)
	}
	for i := 1; i < 20; i++ {
		var value float32
		value = 50.0
		indications := models.Indications{IdSensors: 3, Value: value, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 3, Value: value - value*0.2, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 3, Value: value + value*0.1, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 3, Value: value - value*0.1, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 3, Value: value + value*0.2, Datetime: time.Now()}
		Db.Create(&indications)
	}
	for i := 1; i < 20; i++ {
		var value float32
		value = 100.0
		indications := models.Indications{IdSensors: 4, Value: value, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 4, Value: value - value*0.2, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 4, Value: value + value*0.1, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 4, Value: value - value*0.1, Datetime: time.Now()}
		Db.Create(&indications)
		indications = models.Indications{IdSensors: 4, Value: value + value*0.2, Datetime: time.Now()}
		Db.Create(&indications)
	}
}

func InitializationRegistration() {

	name := "Иван"
	lastName := "Иванов"
	middleName := "Иванович"
	email := "admin@mail.ru"
	password := email
	idRole := 2

	hash, err := tools.HashPassword(password)
	if err != nil {
		fmt.Println(err)
	}

	users := models.Users{Name: name, LastName: lastName, MiddleName: middleName,
		Email: email, IdRole: idRole,
		Password: hash}
	Db.Create(&users)

	name = "Леонард"
	lastName = "Александров"
	middleName = "Федосеевич"
	email = "user@mail.ru"
	password = email
	idRole = 1

	hash, err = tools.HashPassword(password)
	if err != nil {
		fmt.Println(err)
	}

	users = models.Users{Name: name, LastName: lastName, MiddleName: middleName,
		Email: email, IdRole: idRole,
		Password: hash}
	Db.Create(&users)

	users = models.Users{Name: name, LastName: lastName, MiddleName: middleName,
		Email: email, IdRole: idRole,
		Password: hash}
	Db.Create(&users)

	name = "Аввакуум"
	lastName = "Борисович"
	middleName = "Федотов"
	email = "supervisor@mail.ru"
	password = email
	idRole = 3

	hash, err = tools.HashPassword(password)
	if err != nil {
		fmt.Println(err)
	}

	users = models.Users{Name: name, LastName: lastName, MiddleName: middleName,
		Email: email, IdRole: idRole,
		Password: hash}
	Db.Create(&users)
}
