package config

import (
	//"aquaferm/main/controllers"
	"aquaferm/main/models"
	"errors"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var dsn = "root:root@tcp(127.0.0.1:3306)/sensors?charset=utf8mb4&parseTime=True&loc=Local"
var Db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

func DbInitialization() (err error) {

	result := Db.Raw("select ? from INFORMATION_SCHEMA.COLUMNS", "users")
	// проверка ошибки на ErrRecordNotFound
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		Db.AutoMigrate(&models.Users{})
		InitializationRegistration()
	}

	result = Db.Raw("select ? from INFORMATION_SCHEMA.COLUMNS", "roles")
	// проверка ошибки на ErrRecordNotFound
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		Db.AutoMigrate(&models.Roles{})
		roles := models.Roles{Id: 1, Role: "Пользователь"}
		Db.Create(&roles)
		roles = models.Roles{Id: 2, Role: "Админимтратор"}
		Db.Create(&roles)
		roles = models.Roles{Id: 3, Role: "Начальник смены"}
		Db.Create(&roles)
	}

	result = Db.Raw("select ? from INFORMATION_SCHEMA.COLUMNS", "modes")
	// проверка ошибки на ErrRecordNotFound
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		Db.AutoMigrate(&models.Modes{})
	}

	result = Db.Raw("select ? from INFORMATION_SCHEMA.COLUMNS", "equipment")
	// проверка ошибки на ErrRecordNotFound
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		Db.AutoMigrate(&models.Equipment{})
	}

	result = Db.Raw("select ? from INFORMATION_SCHEMA.COLUMNS", "systems")
	// проверка ошибки на ErrRecordNotFound
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		Db.AutoMigrate(&models.Systems{})
		InitializationSystems()
	}

	result = Db.Raw("select ? from INFORMATION_SCHEMA.COLUMNS", "sensors")
	// проверка ошибки на ErrRecordNotFound
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		Db.AutoMigrate(&models.Sensors{})
		InitializationSensors()
	}

	result = Db.Raw("select ? from INFORMATION_SCHEMA.COLUMNS", "subtypes_of_system")
	// проверка ошибки на ErrRecordNotFound
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		Db.AutoMigrate(&models.SubtypesOfSystems{})
	}

	result = Db.Raw("select ? from INFORMATION_SCHEMA.COLUMNS", "types_of_systems")
	// проверка ошибки на ErrRecordNotFound
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		Db.AutoMigrate(&models.TypesOfSystems{})
	}

	result = Db.Raw("select ? from INFORMATION_SCHEMA.COLUMNS", "indications")
	// проверка ошибки на ErrRecordNotFound
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		Db.AutoMigrate(&models.Indications{})
		InitializationIndications()
	}

	result = Db.Raw("select ? from INFORMATION_SCHEMA.COLUMNS", "populations")
	// проверка ошибки на ErrRecordNotFound
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		Db.AutoMigrate(&models.Populations{})
	}

	result = Db.Raw("select ? from INFORMATION_SCHEMA.COLUMNS", "food_populations")
	// проверка ошибки на ErrRecordNotFound
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		Db.AutoMigrate(&models.FoodPopulations{})
	}

	result = Db.Raw("select ? from INFORMATION_SCHEMA.COLUMNS", "population_growths")
	// проверка ошибки на ErrRecordNotFound
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		Db.AutoMigrate(&models.PopulationGrowth{})
	}

	result = Db.Raw("select ? from INFORMATION_SCHEMA.COLUMNS", "tasks")
	// проверка ошибки на ErrRecordNotFound
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		Db.AutoMigrate(&models.Tasks{})
	}

	return nil
}
