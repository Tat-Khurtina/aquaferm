package main

import (
	"aquaferm/main/config"
	"aquaferm/main/controllers"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
)

func main() {
	err := config.DbInitialization()
	if err != nil {
		fmt.Println(err)
	}

	r := mux.NewRouter()
	r.HandleFunc("/", controllers.Home)

	r.HandleFunc("/registration", controllers.Registration)
	r.HandleFunc("/signin", controllers.Signin)
	r.HandleFunc("/logout", controllers.LogoutHandler)
	//r.HandleFunc("/refresh", controllers.Refresh).Methods("POST")

	r.HandleFunc("/sensors", controllers.AddGetSensors)
	r.HandleFunc("/populations", controllers.AddGetPopulations)
	r.HandleFunc("/food", controllers.AddGetFoodPopulations)
	r.HandleFunc("/tasks", controllers.AddGetTasks)
	r.HandleFunc("/indications", controllers.GetIndications)
	//r.HandleFunc("/mail", Mail)

	r.Use(func(h http.Handler) http.Handler {
		return handlers.LoggingHandler(os.Stdout, h)
	})

	// start the server on port 8000
	log.Fatal(http.ListenAndServe(":8000", r))
}
