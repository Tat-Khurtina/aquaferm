package controllers

import (
	"aquaferm/main/config"
	"aquaferm/main/models"
	"aquaferm/main/tools"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

func Registration(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		GetRegistration(w, r)
	} else {
		Register(w, r)
		GetRegistration(w, r)
	}
}

func GetRegistration(w http.ResponseWriter, r *http.Request) {
	var users []models.Users
	var roles []models.Roles

	type ViewUsers struct {
		Users []models.Users
		Role  string
	}

	type ViewData struct {
		Users []models.Users
		Roles []models.Roles
	}

	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	//указываем путь к нужному файлу
	path := filepath.Join(wd, "public", "html", "addGetRegistration.html")
	//создаем html-шаблон
	tmpl, err := template.ParseFiles(path)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	config.Db.Table("users").Scan(&users)
	config.Db.Table("roles").Scan(&roles)

	data := ViewData{
		Users: users,
		Roles: roles,
	}
	err = tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
}

func Register(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	// logic part of log in
	name := r.FormValue("name")
	lastName := r.FormValue("lastName")
	middleName := r.FormValue("middleName")
	email := r.FormValue("email")
	idRoleStr := r.FormValue("idRole")
	password := r.FormValue("password")

	idRole, err := strconv.Atoi(idRoleStr)
	if err != nil {
		fmt.Println(err)
	}

	hash, err := tools.HashPassword(password)
	if err != nil {
		fmt.Println(err)
	}

	users := models.Users{Name: name, LastName: lastName, MiddleName: middleName,
		Email: email, IdRole: idRole,
		Password: hash}
	config.Db.Create(&users)

}
