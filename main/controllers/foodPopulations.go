package controllers

import (
	"aquaferm/main/config"
	"aquaferm/main/models"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

type ViewFoodPopulations struct {
	FoodPopulations []models.FoodPopulations
	Populations     []models.Populations
}

func AddGetFoodPopulations(w http.ResponseWriter, r *http.Request) {
	if true {
		if r.Method != http.MethodPost {
			GetFoodPopulations(w, r)
		} else {
			AddFoodPopulations(w, r)
			GetFoodPopulations(w, r)
		}
	}

}

func GetFoodPopulations(w http.ResponseWriter, r *http.Request) {
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	//указываем путь к нужному файлу
	path := filepath.Join(wd, "public", "html", "addGetFoodPopulations.html")
	//создаем html-шаблон
	tmpl, err := template.ParseFiles(path)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	var populations []models.Populations
	config.Db.Table("populations").Scan(&populations)

	var foodPopulations []models.FoodPopulations
	config.Db.Table("food_populations").Scan(&foodPopulations)
	data := ViewFoodPopulations{
		FoodPopulations: foodPopulations,
		Populations:     populations,
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
}

func AddFoodPopulations(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {

	} else {
		r.ParseForm()

		amountOfFood := r.FormValue("amountOfFood")
		datetimeStr := r.FormValue("datetime")
		idPopulationStr := r.FormValue("idPopulation")
		food := r.FormValue("food")

		datetime, err := time.Parse("2006-01-02", datetimeStr)
		if err != nil {
			fmt.Println(err)
		}

		idPopulation, err := strconv.Atoi(idPopulationStr)
		if err != nil {
			fmt.Println(err)
		}

		foodPopulations := models.FoodPopulations{AmountOfFood: amountOfFood, Datetime: datetime, IdPopulation: idPopulation, Food: food}
		config.Db.Create(&foodPopulations)

	}
}
