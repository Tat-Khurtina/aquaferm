package controllers

import (
	"aquaferm/main/config"
	"aquaferm/main/models"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

var sensors []models.Sensors

type ViewData struct {
	Systems []models.Systems
	Sensors []models.Sensors
}

func AddGetSensors(w http.ResponseWriter, r *http.Request) {
	if true {
		fmt.Println(w.Header())
		if r.Method != http.MethodPost {
			GetSensors(w, r)
		} else {
			AddSensor(w, r)
			GetSensors(w, r)
		}
	}

}

func GetSensors(w http.ResponseWriter, r *http.Request) {
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	//указываем путь к нужному файлу
	path := filepath.Join(wd, "public", "html", "addGetSensors.html")
	//создаем html-шаблон
	tmpl, err := template.ParseFiles(path)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	var systems []models.Systems
	config.Db.Table("systems").Scan(&systems)

	var sensors []models.Sensors
	config.Db.Table("sensors").Scan(&sensors)
	data := ViewData{
		Systems: systems,
		Sensors: sensors,
	}
	//выводим шаблон клиенту в браузер
	err = tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
}

func AddSensor(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		t, _ := template.ParseFiles("login.gtpl")
		t.Execute(w, nil)
	} else {
		r.ParseForm()
		// logic part of log in
		name := r.FormValue("name")
		idSystemsStr := r.FormValue("idSystems")
		location := r.FormValue("location")
		minOperatingConditionStr := r.FormValue("minOperatingCondition")
		maxOperatingConditionStr := r.FormValue("maxOperatingCondition")
		minCriticalConditionStr := r.FormValue("minCriticalCondition")
		maxCriticalConditionStr := r.FormValue("maxCriticalCondition")
		minDeathStr := r.FormValue("minDeath")
		maxDeathStr := r.FormValue("maxDeath")
		unitsOfMeasurement := r.FormValue("unitsOfMeasurement")
		frequency := r.FormValue("frequency")

		idSystems, err := strconv.Atoi(idSystemsStr)
		if err != nil {
			fmt.Println(err)
		}

		minOperatingCondition, err := strconv.ParseFloat(minOperatingConditionStr, 64)
		if err != nil {
			fmt.Println(err)
		}
		maxOperatingCondition, err := strconv.ParseFloat(maxOperatingConditionStr, 64)
		if err != nil {
			fmt.Println(err)
		}
		minCriticalCondition, err := strconv.ParseFloat(minCriticalConditionStr, 64)
		if err != nil {
			fmt.Println(err)
		}
		maxCriticalCondition, err := strconv.ParseFloat(maxCriticalConditionStr, 64)
		if err != nil {
			fmt.Println(err)
		}
		minDeath, err := strconv.ParseFloat(minDeathStr, 64)
		if err != nil {
			fmt.Println(err)
		}
		maxDeath, err := strconv.ParseFloat(maxDeathStr, 64)
		if err != nil {
			fmt.Println(err)
		}

		sensors := models.Sensors{Name: name, IdSystems: idSystems, Location: location, MinOperatingCondition: minOperatingCondition,
			MaxOperatingCondition: maxOperatingCondition, MinCriticalCondition: minCriticalCondition,
			MaxCriticalCondition: maxCriticalCondition, MinDeath: minDeath, MaxDeath: maxDeath,
			UnitsOfMeasurement: unitsOfMeasurement, Frequency: frequency}
		config.Db.Create(&sensors)

	}
}
