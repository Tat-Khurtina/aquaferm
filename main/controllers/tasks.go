package controllers

import (
	"aquaferm/main/config"
	"aquaferm/main/models"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

type ViewTasks struct {
	Users []models.Users
	Tasks []models.Tasks
}

type TasksWithUser struct {
	Tasks    []models.Tasks
	NameUser string
}

func AddGetTasks(w http.ResponseWriter, r *http.Request) {
	ok, currentUser := Welcome(w, r)

	if ok {
		if r.Method != http.MethodPost {
			GetTasks(w, r, currentUser)
		} else {
			AddTask(w, r)
			GetTasks(w, r, currentUser)
		}
	}

}

func GetTasks(w http.ResponseWriter, r *http.Request, currentUser models.Users) {

	switch currentUser.IdRole {
	case 1:
		GetTasksUser(w, r, currentUser)
	case 3:
		GetTasksSupervisor(w, r)
	}

}

func GetTasksSupervisor(w http.ResponseWriter, r *http.Request) {
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	//указываем путь к нужному файлу
	path := filepath.Join(wd, "public", "html", "addGetTasks.html")
	//создаем html-шаблон
	tmpl, err := template.ParseFiles(path)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	var users []models.Users
	config.Db.Table("users").Scan(&users)

	var tasks []models.Tasks
	config.Db.Table("tasks").Scan(&tasks)
	data := ViewTasks{
		Users: users,
		Tasks: tasks,
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
}

func GetTasksUser(w http.ResponseWriter, r *http.Request, currentUser models.Users) {
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	//указываем путь к нужному файлу
	path := filepath.Join(wd, "public", "html", "tasksUser.html")
	//создаем html-шаблон
	tmpl, err := template.ParseFiles(path)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	var tasks []models.Tasks
	config.Db.Table("tasks").Where("id_user = ?", currentUser.Id).Scan(&tasks)
	data := ViewTasks{
		Tasks: tasks,
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
}

func AddTask(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {

	} else {
		r.ParseForm()
		idUserStr := r.FormValue("idUser")
		name := r.FormValue("name")
		description := r.FormValue("description")
		startDateStr := r.FormValue("startDate")
		finishDateStr := r.FormValue("finishDate")

		idUser, err := strconv.Atoi(idUserStr)
		if err != nil {
			fmt.Println(err)
		}

		startDate, err := time.Parse("2006-01-02", startDateStr)
		if err != nil {
			fmt.Println(err)
		}
		finishDate, err := time.Parse("2006-01-02", finishDateStr)
		if err != nil {
			fmt.Println(err)
		}

		tasks := models.Tasks{IdUser: idUser, Name: name, Description: description, StartDate: startDate, FinishDate: finishDate, Status: "Новый"}
		config.Db.Create(&tasks)

	}
}
