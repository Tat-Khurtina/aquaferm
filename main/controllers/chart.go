package controllers

import (
	"net/http"

	"github.com/go-echarts/go-echarts/v2/charts"
	"github.com/go-echarts/go-echarts/v2/opts"
	"github.com/go-echarts/go-echarts/v2/types"
)

func Plotting(w http.ResponseWriter, _ *http.Request, name string, x []int, items []opts.LineData, minOperatingCondition float64,
	maxOperatingCondition float64,
	minCriticalCondition float64,
	maxCriticalCondition float64,
	minDeath float64,
	maxDeath float64) {
	// create a new line instance
	line := charts.NewLine()
	// set some global options like Title/Legend/ToolTip or anything else
	line.SetGlobalOptions(
		charts.WithInitializationOpts(opts.Initialization{Theme: types.ThemeWesteros}),
		charts.WithTitleOpts(opts.Title{
			Title: name,
		}))

	minO := make([]opts.LineData, 0)
	maxO := make([]opts.LineData, 0)
	minC := make([]opts.LineData, 0)
	maxC := make([]opts.LineData, 0)
	minD := make([]opts.LineData, 0)
	maxD := make([]opts.LineData, 0)
	for i := 0; i < len(x); i++ {
		minO = append(minO, opts.LineData{Value: minOperatingCondition})
		maxO = append(maxO, opts.LineData{Value: maxOperatingCondition})
		minC = append(minC, opts.LineData{Value: minCriticalCondition})
		maxC = append(maxC, opts.LineData{Value: maxCriticalCondition})
		minD = append(minD, opts.LineData{Value: minDeath})
		maxD = append(maxD, opts.LineData{Value: maxDeath})
	}

	l := line.SetXAxis(x)

	l.AddSeries("Рабочее", minO).AddSeries("Рабочее", maxO).
		SetSeriesOptions(charts.WithLineStyleOpts(opts.LineStyle{
			Color: "green",
		}))

	l.AddSeries("Критическое", minC).AddSeries("Критическое", maxC).
		SetSeriesOptions(charts.WithItemStyleOpts(opts.ItemStyle{
			Color: "orange",
		}))

	l.AddSeries("Смерть", minD).AddSeries("Смерть", maxD).
		SetSeriesOptions(charts.WithItemStyleOpts(opts.ItemStyle{
			Color: "red",
		}))

	l.AddSeries("", items).
		SetSeriesOptions(charts.WithLineChartOpts(opts.LineChart{Smooth: true}))

	line.Render(w)
}
