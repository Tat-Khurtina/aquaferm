package controllers

import (
	"aquaferm/main/config"
	"aquaferm/main/models"
	"aquaferm/main/tools"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

func Signin(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		FormSignin(w, r)
	} else {
		ValidationSignin(w, r)
	}
}

func FormSignin(w http.ResponseWriter, r *http.Request) {
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	//указываем путь к нужному файлу
	path := filepath.Join(wd, "public", "html", "signin.html")
	//создаем html-шаблон
	tmpl, err := template.ParseFiles(path)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	//выводим шаблон клиенту в браузер
	err = tmpl.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
}

func ValidationSignin(w http.ResponseWriter, r *http.Request) {
	var user models.Users

	email := r.FormValue("email")
	enteredPassword := r.FormValue("password")

	config.Db.Table("users").Where("email = ?", email).Scan(&user)

	match := tools.CheckPasswordHash(enteredPassword, user.Password)
	fmt.Println("Match:   ", match)
	if !match {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	// Declare the expiration time of the token
	// here, we have kept it as 5 minutes
	//expirationTime := time.Now().Add(5 * time.Minute)
	expirationTime := time.Now().Add(9 * time.Hour)
	// Create the JWT claims, which includes the username and expiry time
	claims := &Claims{
		CurrentUser: user,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
		},
	}

	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		// If there is an error in creating the JWT return an internal server error
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Finally, we set the client cookie for "token" as the JWT we just generated
	// we also set an expiry time which is the same as the token itself
	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   tokenString,
		Expires: expirationTime,
	})
	http.Redirect(w, r, "/", http.StatusSeeOther)

}
