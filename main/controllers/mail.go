package controllers

import (
	"fmt"
	"net/smtp"
)

func Mail(message string) error {
	// Sender data.
	from := "TestEcoferm@gmail.com"
	password := "TestEcoferm12345678"
	// Receiver email address.
	to := []string{
		//	"secondemail@gmail.com",
	}
	// smtp server configuration.
	smtpServer := smtpServer{host: "smtp.gmail.com", port: "587"}
	// Message.
	messageByte := []byte(message)
	// Authentication.
	auth := smtp.PlainAuth("", from, password, smtpServer.host)
	// Sending email.
	err := smtp.SendMail(smtpServer.Address(), auth, from, to, messageByte)
	if err != nil {
		return err
	}
	fmt.Println("Email Sent!")
	return nil
}

// smtpServer data to smtp server
type smtpServer struct {
	host string
	port string
}

// Address URI to smtp server
func (s *smtpServer) Address() string {
	return s.host + ":" + s.port
}
