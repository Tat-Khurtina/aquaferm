package controllers

import (
	"aquaferm/main/config"
	"fmt"
	"github.com/go-echarts/go-echarts/v2/opts"
	"net/http"
	"time"
)

func GetIndications(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<title>Показания датчиков</title><link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css'</head><body></body>",
		nil)

	//var idSensors []int
	var idSensor int
	//var nameSensors []string
	var nameSensor string
	var minOperatingCondition float64
	var maxOperatingCondition float64
	var minCriticalCondition float64
	var maxCriticalCondition float64
	var minDeath float64
	var maxDeath float64

	rowsSensors, _ := config.Db.Table("sensors").Select("id", "name",
		"min_operating_condition", "max_operating_condition",
		"min_critical_condition", "max_critical_condition",
		"min_death", "max_death").Rows()
	defer rowsSensors.Close()
	for rowsSensors.Next() {
		rowsSensors.Scan(&idSensor, &nameSensor,
			&minOperatingCondition, &maxOperatingCondition,
			&minCriticalCondition, &maxCriticalCondition,
			&minDeath, &maxDeath)

		var values []float64
		var value float64
		var times []int
		var t time.Time

		rows, _ := config.Db.Table("indications").Select("value", "datetime").Where("id_sensors = ?", idSensor).Rows()
		defer rows.Close()
		for rows.Next() {
			rows.Scan(&value, &t)
			values = append(values, value)
			times = append(times, t.Second())
		}

		fmt.Println("values", values)
		fmt.Println("times", times)
		items := make([]opts.LineData, 0)
		for _, v := range values {
			items = append(items, opts.LineData{Value: v})
		}

		Plotting(w, r, nameSensor, times, items,
			minOperatingCondition,
			maxOperatingCondition,
			minCriticalCondition,
			maxCriticalCondition,
			minDeath,
			maxDeath)

	}
}
