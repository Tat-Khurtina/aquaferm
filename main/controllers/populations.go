package controllers

import (
	"aquaferm/main/config"
	"aquaferm/main/models"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

type ViewPopulations struct {
	Populations []models.Populations
}

var populations []models.Populations

func AddGetPopulations(w http.ResponseWriter, r *http.Request) {
	if true {
		if r.Method != http.MethodPost {
			GetPopulations(w, r)
		} else {
			AddPopulation(w, r)
			GetPopulations(w, r)
		}
	}

}

func GetPopulations(w http.ResponseWriter, r *http.Request) {
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	//указываем путь к нужному файлу
	path := filepath.Join(wd, "public", "html", "addGetPopulations.html")
	//создаем html-шаблон
	tmpl, err := template.ParseFiles(path)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	var populations []models.Populations
	config.Db.Table("populations").Scan(&populations)
	data := ViewPopulations{
		Populations: populations,
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
}

func AddPopulation(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {

	} else {
		r.ParseForm()
		// logic part of log in
		name := r.FormValue("name")
		startDateStr := r.FormValue("startDate")
		comment := r.FormValue("сomment")

		startDate, err := time.Parse("2006-01-02", startDateStr)
		if err != nil {
			fmt.Println(err)
		}

		populations := models.Populations{Name: name, StartDate: startDate, Comment: comment}
		config.Db.Create(&populations)

	}
}
