package controllers

import (
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

func Home(w http.ResponseWriter, r *http.Request) {
	ok, currentUser := Welcome(w, r)

	if ok {
		FormHome(w, r, currentUser.IdRole)
	} else {
		Signin(w, r)
	}
}

func FormHome(w http.ResponseWriter, r *http.Request, idRole int) {
	homeHtml := ""
	switch idRole {
	case 1:
		homeHtml = "homeUser.html"
	case 2:
		homeHtml = "homeAdmin.html"
	case 3:
		homeHtml = "homeSupervisor.html"
	}
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	//указываем путь к нужному файлу
	path := filepath.Join(wd, "public", "html", homeHtml)
	//создаем html-шаблон
	tmpl, err := template.ParseFiles(path)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	//выводим шаблон клиенту в браузер
	err = tmpl.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
}
