module aquaferm

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-echarts/go-echarts/v2 v2.2.4
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	golang.org/x/crypto v0.0.0-20220408190544-5352b0902921
	gorm.io/driver/mysql v1.3.2
	gorm.io/gorm v1.23.1
)
